import { useEffect, useState } from "react";

function Count() {
    const [count, setCount] = useState(0);

    const increaseCount = () => {
        console.log("increase count ...");
        setCount(count + 1);
    }

    //sự kiện xẩy ra 1 lần: componentDidMount
    useEffect(() => {
        console.log("componentDidMount ...");
        document.title = `You clicked ${count} times!`;
    }, [])

    //sự kiện xẩy ra mỗi khi render lại component: componentDidUpdate
    useEffect(() => {
        console.log("componentDidUpdate ...");
        document.title = `You clicked ${count} times!`;
        return () => {
            //sự kiện xẩy ra khi component biến mất
            console.log("componentWillUnmout ...");
        }
    })

    return (
        <div>
            <p>You clicked {count} times!</p>
            <button onClick={increaseCount}>Click Me!</button>
        </div>
    )
}

export default Count